package testCases.booking;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import testCases.user.PostUserPositive;
import static io.restassured.RestAssured.given;


public class GetBookingPositive {

    @Test(description = "Valid booking retrieval scenario with correct date")
    public void getBookingsWithDate(){
        Response res =
            given()
            .when()
                .get("http://127.0.0.1:8900/booking?date=2021-09-12")
            .then()
                .statusCode(200)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString.contains("2021-09-12"), true);

    }

    @Test(description = "Valid booking retrieval scenario with correct Id")
    public void getBookingsWithId(){
        Response res =
            given()
            .when()
                .get("http://127.0.0.1:8900/booking?id=" + PostUserPositive.getUserId())
            .then()
                .statusCode(200)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString.contains(PostUserPositive.getUserId()), true);
    }

    @Test(description = "Valid booking retrieval scenario with correct Id and date")
    public void getBookingsWithIdAndDate(){
        Response res =
            given()
            .when()
                .get("http://127.0.0.1:8900/booking?id="+ PostUserPositive.getUserId() +"&date=2021-09-12")
            .then()
                .statusCode(200)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString.contains("2021-09-12"), true);
        Assert.assertEquals(jsonString.contains(PostUserPositive.getUserId()), true);
    }
}
