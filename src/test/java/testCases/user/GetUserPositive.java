package testCases.user;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class GetUserPositive {

    @Test(description = "Valid all users retrieval scenario")
    public void getAllUsers(){
        given()
        .when()
            .get("http://127.0.0.1:8900/user/all")
        .then()
            .statusCode(200)
            .log().body();

    }

    @Test(description = "Valid user retrieval scenario with correct id")
    public void getUser(){

        Response res =
            given()
            .when()
                .get("http://127.0.0.1:8900/user?id=" + PostUserPositive.getUserId())
            .then()
                .statusCode(200)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(res.path("id"), PostUserPositive.getUserId());
    }
}
