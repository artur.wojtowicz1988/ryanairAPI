package testCases.user;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.HashMap;
import static io.restassured.RestAssured.given;

public class PostUserPositive {

    public static String getUserId() {
        return userId;
    }

    public static String userId;

    @Test(description = "Correct user creation with valid email and name")
    public void postUser(){

        HashMap data = new HashMap();
        data.put("email", "newuser1@gmail.com");
        data.put("name", "Adam Adamski");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/user")
            .then()
                .statusCode(201)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString.contains("Adam Adamski"), true);
        userId = res.path("id");

    }
}
