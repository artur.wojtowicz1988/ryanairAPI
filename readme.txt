Documentation for RyanairAPI

1. I chose JAVA 1.8 with maven nad TestNG for API tests, because I am most familiar with those tools.
2. Tests are done with Rest-Assured, because of:
	- designes for API Automation Testing
	- easy integration with TestNG
	- support BDD behaviour driven test cases
	- built in library of Assertion

3. How to RUN tests:
Go to repository directory through CMD/console and post: "mvn test".

4. How to open report:
Go to test-output directory under repository and open "index.html".